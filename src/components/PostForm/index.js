import React from 'react';

const PostForm = (props) => {
  //Local props
  const { titleText, bodyText, handleChangeTitle, handleChangeBody, handleSubmitForm } = props;
  return (
    <>
      <h1>Add New Post</h1>
      <form onSubmit={handleSubmitForm}>
        <input type="text" name="body" onChange={handleChangeBody} value={bodyText} />
        <input type="text" name="title" onChange={handleChangeTitle} value={titleText} />
        <input type="submit" value="Add post" />
      </form>
    </>
  )
};

export default PostForm;
