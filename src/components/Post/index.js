import React from 'react';
import PropTypes from 'prop-types';
import './_styles/style.css';

const Post = (props) => {
  //Local props
  const { products } = props;
  return (
    <table>
      <thead>
        <tr>
          <td>Id</td>
          <td>Body</td>
          <td>Title</td>
          <td>UserId</td>
        </tr>
      </thead>
      <tbody>
        {products && products.length > 0 && products.map((item, index) => {
          return (
            <tr key={index}>
              <td>{item.id}</td>
              <td>{item.body}</td>
              <td>{item.title}</td>
              <td>{item.userId}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

Post.propTypes = {
  products: PropTypes.array,
};

export default Post;
