import { combineReducers } from 'redux';
import { productStore } from './post/reducers'

const store = combineReducers({
  product: productStore
});

export default store
