import * as types from './constants'

export const acGetListProduct = (data) => {
  data.reverse();
  return {
    type: types.GET_LIST_PRODUCT,
    payload: {
      listProduct: data
    }
  }
}

export const acAddNewPost = (data) => {
  return {
    type: types.ADD_NEW_POST,
    payload: {
      dataNew: data
    }
  }
}