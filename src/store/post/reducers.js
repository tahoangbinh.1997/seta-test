import * as types from './constants';

const initialState = {
  listProduct: null
};

export const productStore = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_LIST_PRODUCT: {
      return {
        listProduct: action.payload.listProduct,
      };
    }
    case types.ADD_NEW_POST: {
      state.listProduct.unshift(action.payload.dataNew)
      return {
        listProduct: [ ...state.listProduct ]
      }
    }
    default:
      return { ...state };
  }
};
