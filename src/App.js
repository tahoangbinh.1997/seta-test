import React, { useState, useEffect } from 'react';
import ajax from './utils/axios';
import './App.css';
import Post from './components/Post/index';
import PostForm from './components/PostForm/index'
import { useDispatch, useSelector } from 'react-redux';
import { acAddNewPost, acGetListProduct } from './store/post/actions'

const App = () => {
  //Hooks
  const products = useSelector(state => state.product.listProduct);
  const dispatch = useDispatch();

  //Local state
  const [titleText, setTitleText] = useState('');
  const [bodyText, setBodyText] = useState('');
  
  //useEffects
  useEffect(() => {
    handleGetProducts();
  }, []);

  //Logic
  const handleGetProducts = async () => {
    const data = await ajax.get('/posts');
    if (data.data) {
      dispatch(acGetListProduct(data.data));
    }
  }

  const handleChangeTitle = (event) => {
    setTitleText(event.target.value);
  }

  const handleChangeBody = (event) => {
    setBodyText(event.target.value);
  }

  const handleSubmitForm = (event) => {
    event.preventDefault();
    const dataNew = {
      id: products.length + 1,
      title: titleText,
      body: bodyText,
      userId: Math.ceil(((products.length + 1) / 10).toPrecision(3))
    };
    dispatch(acAddNewPost(dataNew));
  }

  return (
    <div className="App">
      <PostForm titleText={titleText} bodyText={bodyText} handleChangeTitle={handleChangeTitle} handleChangeBody={handleChangeBody} handleSubmitForm={handleSubmitForm} />
      <Post products={products} />
    </div>
  );
}

export default App;
