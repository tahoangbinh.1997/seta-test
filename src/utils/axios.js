import axios from "axios";
const URL_API = "https://jsonplaceholder.typicode.com";
const API_URL = URL_API;
let myAxios = axios.create({
  baseURL: API_URL,
  timeout: 20000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "**",
    "Access-Control-Allow-Methods": "POST, PUT, GET, OPTIONS, DELETE",
    "Access-Control-Allow-Headers": "X-Requested-With, X-Auth-Token"
  }
});

const post = (url, data, config = {}) => {
  return myAxios.post(url, data, config);
};

const get = (url, data, config = {}) => {
  return myAxios.get(url, config);
};

const put = (url, data, config = {}) => {
  return myAxios.put(url, data, config);
};

const patch = (url, data, config = {}) => {
  return myAxios.patch(url, data, config);
};

const del = (url, config = {}) => {
  return myAxios.delete(url, config);
};

myAxios.interceptors.request.use(
  function (req) {
    req.time = { startTime: new Date() };
    return req;
  },
  (err) => {
    return Promise.reject(err);
  }
);

myAxios.interceptors.response.use(
  function (res) {
    return res;
  },
  (err) => {
    if (err.response.status === 404) {
      console.log("Not found");
      console.log(err.response.status);
    }
    if (err.response.status === 400) {
      console.log("Error 400");
    }
    if (err.response.status === 500) {
      alert("Server Error");
    }
    // return Promise.reject(err);
  }
);

const ajax = {
  post,
  get,
  put,
  patch,
  delete: del
};

export default ajax;
