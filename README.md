# React test

Provide the API for posts: https://jsonplaceholder.typicode.com/posts

Writing a react-redux app that:

- Get and display posts from API

- Have a PostForm component to add new post
